# JAX-RS Application

This is a RESTful JAX-RS wrapper around the Venn solver. 
    
## Running the application locally

First build it with Maven:

    $ mvn clean install

Then run it with:

    $ java -cp 'target/classes:target/dependency/*' com.venngroups.Main

The solver can be started with an http call like:

    http://localhost:8080/services/solver?vennue=1&verbose=1&write=0

## Running the application remotely

    $ git push heroku
