package com.venngroups.services;

import java.util.Date;
import java.text.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.venngroups.models.Solver;

@Path("/solver")
@Produces(MediaType.APPLICATION_JSON)
public class SolverService {

    @GET
    public Response get(@QueryParam("vennue") int vennue,
                        @DefaultValue("") @QueryParam("date") String dateInString,
                        @DefaultValue("0") @QueryParam("verbose") int verbose,
                        @DefaultValue("0") @QueryParam("emails") int emails,
                        @DefaultValue("1") @QueryParam("write") int write,
                        @DefaultValue("0") @QueryParam("warn") int warn,
                        @DefaultValue("") @QueryParam("seed") String seed,
                        @DefaultValue("") @QueryParam("solution") String solution
                        ) 
    {
      final int vennueID = vennue;
      final boolean isVerbose = (verbose == 1);
      final boolean sendEmails = (emails == 1);
      final boolean writeDB = (write == 1);
      final boolean sendWarnings = (warn == 1);
      final String seedString = seed;
      final String solutionString = solution;
      Date parseDate = null;
      if (!dateInString.trim().equals("")) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
          parseDate = formatter.parse(dateInString);
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }
      final Date date = parseDate;
      Thread one = new Thread() {
        int thisVennue = vennueID;
        public void run() {
          Solver solver = new Solver(thisVennue, date);
          solver.solve(isVerbose, writeDB, sendEmails, sendWarnings, date, seedString, solutionString);
        }  
      };
      one.start();
      return Response.status(Response.Status.ACCEPTED).build();
    }

}

