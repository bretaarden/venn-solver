package com.venngroups.services;

import com.venngroups.models.Solver;

import java.io.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.sun.jersey.multipart.*;
import com.sun.jersey.core.header.*;

@Path("/xmlsolver")
public class XMLSolverService {

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response post(
        @DefaultValue("0") @QueryParam("verbose") int verbose,
        @DefaultValue("0") @QueryParam("emails") int emails,
        @FormDataParam("file") final InputStream uploadedInputStream,
        @FormDataParam("file") FormDataContentDisposition fileDetail
      )
    {
      final boolean isVerbose = (verbose == 1);
      final boolean sendEmails = (emails == 1);
      // Read the file data for the thread before closing the client connection
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[] buffer = new byte[1024];
      int len;
      try {
        while ((len = uploadedInputStream.read(buffer)) > -1 ) {
            baos.write(buffer, 0, len);
        }
        baos.flush();
      } catch (IOException e) {
        System.out.println(e);
      }
      final InputStream input = new ByteArrayInputStream(baos.toByteArray()); 
      Thread one = new Thread() {
        public void run() {
          Solver solver = new Solver(input);
          solver.solve(isVerbose, false, sendEmails, false, null, null, null);
        }  
      };
      one.start();
      return Response.status(Response.Status.ACCEPTED).build();
    }

}

