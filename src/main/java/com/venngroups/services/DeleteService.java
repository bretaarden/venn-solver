package com.venngroups.services;

import java.util.Date;
import java.text.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.venngroups.models.MySQLWriter;

@Path("/delete")
//@Produces(MediaType.APPLICATION_JSON)
public class DeleteService {

    @GET
    public Response get(@QueryParam("vennue") String vennue,
                        @DefaultValue("") @QueryParam("date") String dateInString
                      ) 
    {
      Date parseDate = null;
      if (!dateInString.trim().equals("")) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
          parseDate = formatter.parse(dateInString);
        } catch (ParseException e) {
          e.printStackTrace();
        }
      }
      final Date date = parseDate;
      MySQLWriter writer = new MySQLWriter();
      writer.deleteSolution(Integer.parseInt(vennue), date);
      return Response.status(Response.Status.ACCEPTED).build();
    }

}

