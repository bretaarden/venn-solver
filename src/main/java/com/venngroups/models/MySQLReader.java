package com.venngroups.models;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.math.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import com.google.gson.*;
import org.apache.commons.codec.binary.Base64;
import org.uncommons.maths.statistics.DataSet;
import com.venngroups.models.config.*;

public class MySQLReader
{
  private static Connection conn;
  private static double
    targetMean = 0.5,
    targetStdDev = 0.25;

  public MySQLReader() {
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (Exception ex) {
      System.out.println("Unable to load MySQL driver: " + ex);
    }
    String dbServer = System.getenv("DB_SERVER");
    String dbDatabase = System.getenv("DB_DATABASE");
    String dbUser = System.getenv("DB_USER");
    String dbPassword = System.getenv("DB_PASSWORD");
    String connStr = String.format("jdbc:mysql://%s/%s?user=%s&password=%s", dbServer, dbDatabase, dbUser, dbPassword);
    try {
      conn = DriverManager.getConnection(connStr);
    } catch (SQLException ex) {
      System.out.println("Exception: " + ex.getMessage());
    }
  }

  public boolean hasExistingSolution(int vennueID, java.util.Date date) {
    boolean solutionExists = true;
    String sql = "SELECT 1 FROM tblsolutionactivity WHERE postsolver = 1 AND meetupID = ? AND date = ? LIMIT 1;";
    if (date == null) {
      Calendar calendar = Calendar.getInstance();
      date = new java.util.Date(calendar.getTime().getTime());
    }
    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
    try {
      CallableStatement stmt = conn.prepareCall(sql);
      stmt.setInt(1, vennueID);
      stmt.setDate(2, sqlDate);

      boolean results = stmt.execute();
      if (results) {
        ResultSet rs = stmt.getResultSet();
        solutionExists = rs.next();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return solutionExists;
  }

  public Config readConfig(int vennueID, java.util.Date date) {
    List<List<Map<String, Object>>> dbConfigs = executeProcedure(vennueID, date);

    Config config = new Config();
    loadGroupProposals(config, dbConfigs.get(0));
    loadProposals(config, dbConfigs.get(1));
    loadActivities(config, dbConfigs.get(2));
    loadActivityRatings(config, dbConfigs.get(3));
    loadRelationships(config, dbConfigs.get(4));
    loadActivityDiversity(config, dbConfigs.get(5));
    loadPlayerDiversity(config, dbConfigs.get(6));
    loadSolutionMismatch(config, dbConfigs.get(7));
    loadActivityCategories(config, dbConfigs.get(8));
    loadWorkgroups(config, dbConfigs.get(9));
    loadCommittedActivities(config, dbConfigs.get(10));
    normalizeRatings(config);
    return config;
  }

  // Correct for heteroscedasticity
  private void normalizeRatings(Config config) {
    List<ParticipantConfig> userList = config.getParticipants().getParticipantConfig();
    for (ParticipantConfig user : userList) {
      ParticipantChoices choices = user.getParticipantChoices();
      List<ParticipantChoiceConfig> activityList = choices.getParticipantChoiceConfig();
      List<ParticipantGroupConfig> groupList = choices.getParticipantGroupConfig();
      DataSet set = new DataSet(activityList.size() + groupList.size());

      for (ParticipantChoiceConfig activity : activityList) {
        BigDecimal attractivenessObject = activity.getAttractiveness();
        if (attractivenessObject == null)
          continue;
        double rating = attractivenessObject.doubleValue();
        if (rating == 0.0) continue;
        set.addValue(rating);
      }

      for (ParticipantGroupConfig group : groupList) {
        BigDecimal attractivenessObject = group.getAttractiveness();
        if (attractivenessObject == null)
          continue;
        double rating = attractivenessObject.doubleValue();
        if (rating == 0.0) continue;
        set.addValue(rating);
      }

      if (set.getSize() == 0) continue;
      double mean = set.getArithmeticMean();
      double stdDev = set.getStandardDeviation();
      if (stdDev == 0) stdDev = targetStdDev;

      for (ParticipantChoiceConfig activity : activityList) {
        BigDecimal attractivenessObject = activity.getAttractiveness();
        if (attractivenessObject == null)
          continue;
        double rating = attractivenessObject.doubleValue();
        if (rating == 0.0) continue;
        rating = targetMean + (rating - mean) * targetStdDev / stdDev;
        rating *= 5.0;
        if (rating < 0.01) rating = 0.01;
        if (rating > 0.99) rating = 0.99;
        activity.setAttractiveness(new BigDecimal(rating));
      }

      for (ParticipantGroupConfig group : groupList) {
        BigDecimal attractivenessObject = group.getAttractiveness();
        if (attractivenessObject == null)
          continue;
        double rating = attractivenessObject.doubleValue();
        if (rating == 0.0) continue;
        rating = targetMean + (rating - mean) * targetStdDev / stdDev;
        if (rating < 0.01) rating = 0.01;
        if (rating > 0.99) rating = 0.99;
        group.setAttractiveness(new BigDecimal(rating));
      }
    }
  }

  private void loadGroupProposals(Config config, List<Map<String, Object>> proposals) {
    config.setParticipants(new Participants());
    for (Map<String, Object> row : proposals) {
      int userID = (Integer) row.get("UserID");
      int groupID = (Integer) row.get("GroupID");
      BigDecimal rating = (BigDecimal) row.get("Rating");
      long startTime = (Long) row.get("StartTime");
      ParticipantConfig user = getParticipantConfig(config, userID);
      ParticipantGroupConfig choice = getParticipantGroupConfig(user, groupID);
      choice.setAttractiveness(rating);
      choice.setStartTime(BigInteger.valueOf(startTime));
    }
  }

  private void loadProposals(Config config, List<Map<String, Object>> proposals) {
    for (Map<String, Object> row : proposals) {
      int userID = (Integer) row.get("userid");
      int activityID = (Integer) row.get("activityid");
      int roleID = (Integer) row.get("roleID");
      long startTime = (Integer) row.get("StartTime");
      BigDecimal rating = (BigDecimal) row.get("rating");
      ParticipantConfig user = getParticipantConfig(config, userID);
      ParticipantChoiceConfig choice = getParticipantChoiceConfig(user);
      choice.setActivityID(BigInteger.valueOf(activityID));
      choice.setAttractiveness(rating);
      choice.setRoleID(BigInteger.valueOf(roleID));
      choice.setStartTime(BigInteger.valueOf(startTime));
    }
  }

  private void loadActivities(Config config, List<Map<String, Object>> activities) {
    config.setActivities(new Activities());
    for (Map<String, Object> row : activities) {
      int activityID = (Integer) row.get("activityid");
      int groupID = (Integer) row.get("groupID");
      int minNumPlayers = (Integer) row.get("minNumPlayers");
      int maxNumPlayers = (Integer) row.get("maxNumPlayers");
      int maxInstances = (Integer) row.get("maxInstances");
      int duration = (Integer) row.get("duration");
      Long needsCoordinator = (Long) row.get("needsCoordinator");
      ActivityConfig activity = getActivityConfig(config, activityID);
      activity.setGroupID(BigInteger.valueOf(groupID));
      activity.setMinParticipants(BigInteger.valueOf(minNumPlayers));
      activity.setMaxParticipants(BigInteger.valueOf(maxNumPlayers));
      activity.setMaxInstances(BigInteger.valueOf(maxInstances));
      // Minutes -> milliseconds
      activity.setDuration(BigInteger.valueOf(duration * 60 * 1000));
      activity.setNeedsCoordinator(BigInteger.valueOf(needsCoordinator));
    }
  }

  private void loadActivityRatings(Config config, List<Map<String, Object>> funness) {
    for (Map<String, Object> row : funness) {
      int activityID = (Integer) row.get("activityid");
      int count = (Integer) row.get("Size");
      BigDecimal fun = (BigDecimal) row.get("fun");
      ActivityConfig activity = getActivityConfig(config, activityID);
      ActivityRatings ratings = activity.getActivityRatings();
      if (ratings == null) {
        ratings = new ActivityRatings();
        activity.setActivityRatings(ratings);
      }
      List<ActivityRatingConfig> list = ratings.getActivityRatingConfig();
      ActivityRatingConfig rating = new ActivityRatingConfig();
      rating.setParticipantCount(BigInteger.valueOf(count));
      rating.setRating(fun);
      list.add(rating);
    }
  }

  private void loadRelationships(Config config, List<Map<String, Object>> users) {
    config.setPlayerPairs(new PlayerPairs());
    List<PlayerPair> list = config.getPlayerPairs().getPlayerPair();
    for (Map<String, Object> row : users) {
      int userID1 = (Integer) row.get("userID");
      ParticipantConfig user = getParticipantConfig(config, userID1);
      Integer minPlayers = (Integer) row.get("minplayers");
      if (minPlayers != null)
        user.setMinPlayers(BigInteger.valueOf(minPlayers));
      Integer allowNonSolution = (Integer) row.get("allownonsolution");
      if (allowNonSolution == null)
        allowNonSolution = 0;
      user.setAllowNonSolutions(BigInteger.valueOf(allowNonSolution));
      Integer tentative = (Integer) row.get("tentative");
      if (tentative == null)
        tentative = 0;
      user.setTentative(BigInteger.valueOf(tentative));
      Integer workgroup = (Integer) row.get("workgroup_id");
      if (workgroup == null)
        workgroup = 0;
      user.setWorkgroupID(BigInteger.valueOf(workgroup));
      Integer limit_workgroup = (Integer) row.get("limit_workgroup_id");
      if (limit_workgroup == null)
        limit_workgroup = 0;
      user.setWorkgroupLimitID(BigInteger.valueOf(limit_workgroup));

      byte[] relationBytes = (byte[]) row.get("relationships");
      if (relationBytes == null)
        continue;
      String relationships = decrypt(relationBytes);
      JsonElement relationObject = (new JsonParser().parse(relationships));
      if (!relationObject.isJsonObject())
        continue;
      JsonObject friendFoes = relationObject.getAsJsonObject();
      for (Map.Entry<String,JsonElement> entry : friendFoes.entrySet()) {
        int userID2 = Integer.parseInt(entry.getKey());
        JsonObject data = entry.getValue().getAsJsonObject();
        JsonElement friendFoeElement = data.get("friendfoe");
        if (friendFoeElement == null)
          continue;
        String friendFoeString = friendFoeElement.getAsString();
        BigInteger friendFoe = null;
        if (friendFoeString.equals("friend")) {
          friendFoe = ActivityInstance.FRIEND;
        } else if (friendFoeString.equals("foe")) {
          friendFoe = ActivityInstance.FOE;
        }
        if (friendFoe != null) {
          PlayerPair pair = new PlayerPair();
          pair.setUserID1(BigInteger.valueOf(userID1));
          pair.setUserID2(BigInteger.valueOf(userID2));
          pair.setType(friendFoe);
          list.add(pair);
        }
      }
    }
  }

  private void loadActivityDiversity(Config config, List<Map<String, Object>> users) {
    for (Map<String, Object> row : users) {
      int userID = (Integer) row.get("userID");
      ParticipantConfig user = getParticipantConfig(config, userID);
      int activityID = (Integer) row.get("activityID");
      user.setRecentActivityID(BigInteger.valueOf(activityID));
    }
  }

  private void loadActivityCategories(Config config, List<Map<String, Object>> activities) {
    for (Map<String, Object> row : activities) {
      int activityID = (Integer) row.get("activityID");
      int categoryID = (Integer) row.get("category_id");
      ActivityConfig activity = getActivityConfig(config, activityID);
      ActivityCategories categoryConfig = activity.getActivityCategories();
      if (categoryConfig == null) {
        categoryConfig = new ActivityCategories();
        activity.setActivityCategories(categoryConfig);
      }
      List<BigInteger> categories = categoryConfig.getActivityCategory();
      categories.add(BigInteger.valueOf(categoryID));
    }
  }

  private void loadSolutionMismatch(Config config, List<Map<String, Object>> users) {
    for (Map<String, Object> row : users) {
      int userID = (Integer) row.get("userID");
      ParticipantConfig user = getParticipantConfig(config, userID);
      double solutionMismatch = (Double) row.get("solutionMismatch");
      user.setRecentSolutionMismatch(BigDecimal.valueOf(solutionMismatch));
    }
  }

  private void loadPlayerDiversity(Config config, List<Map<String, Object>> players) {
    // ra.userID as userID1, sau.userID as userID2
    List<PlayerPair> list = config.getPlayerPairs().getPlayerPair();
    for (Map<String, Object> row : players) {
      int userID1 = (Integer) row.get("userID1");
      int userID2 = (Integer) row.get("userID2");
      PlayerPair pair = new PlayerPair();
      pair.setUserID1(BigInteger.valueOf(userID1));
      pair.setUserID2(BigInteger.valueOf(userID2));
      pair.setType(ActivityInstance.PLAYER_DIVERSITY);
      list.add(pair);
    }
  }

  private void loadWorkgroups(Config config, List<Map<String, Object>> workgroups) {
    // ra.userID as userID1, sau.userID as userID2
    config.setWorkgroups(new Workgroups());
    List<Workgroup> list = config.getWorkgroups().getWorkgroup();
    for (Map<String, Object> row : workgroups) {
      int workgroupID = (Integer) row.get("id");
      int parentID = (Integer) row.get("parent_id");
      Workgroup workgroup = new Workgroup();
      workgroup.setWorkgroupID(BigInteger.valueOf(workgroupID));
      workgroup.setParentID(BigInteger.valueOf(parentID));
      list.add(workgroup);
    }
  }

  private void loadCommittedActivities(Config config, List<Map<String, Object>> activities) {
    for (Map<String, Object> row : activities) {
      int userID = (Integer) row.get("UserID");
      Integer activityID = (Integer) row.get("ActivityID");
      Long time = (Long) row.get("Time");
      Integer instance = (Integer) row.get("Instance");
      CommittedActivity activity = new CommittedActivity();
      activity.setActivityID(BigInteger.valueOf(activityID));
      activity.setStartTime(BigInteger.valueOf(time));
      activity.setInstance(BigInteger.valueOf(instance));
      ParticipantConfig user = getParticipantConfig(config, userID);
      user.setCommittedActivity(activity);
    }
  }

  private ActivityConfig getActivityConfig(Config config, int activityID) {
    List<ActivityConfig> list = config.getActivities().getActivityConfig();
    ActivityConfig activity = null;
    for (ActivityConfig a : list) {
      if (a.getActivityID().intValue() == activityID) {
        activity = a;
        break;
      }
    }
    if (activity == null) {
      activity = new ActivityConfig();
      activity.setActivityID(BigInteger.valueOf(activityID));
      list.add(activity);
    }
    return activity;
  }

  private ParticipantConfig getParticipantConfig(Config config, int userID) {
    List<ParticipantConfig> list = config.getParticipants().getParticipantConfig();
    ParticipantConfig user = null;
    for (ParticipantConfig p : list) {
      if (p.getUserID().intValue() == userID) {
        user = p;
        break;
      }
    }
    if (user == null) {
      user = new ParticipantConfig();
      user.setUserID(BigInteger.valueOf(userID));
      list.add(user);
    }
    return user;
  }

  private ParticipantChoiceConfig getParticipantChoiceConfig(ParticipantConfig user) {
    ParticipantChoices choices = user.getParticipantChoices();
    if (choices == null) {
      choices = new ParticipantChoices();
      user.setParticipantChoices(choices);
    }
    List<ParticipantChoiceConfig> list = choices.getParticipantChoiceConfig();
    ParticipantChoiceConfig choice = new ParticipantChoiceConfig();
    list.add(choice);
    return choice;
  }

  private ParticipantGroupConfig getParticipantGroupConfig(ParticipantConfig user, int groupID) {
    ParticipantChoices choices = user.getParticipantChoices();
    if (choices == null) {
      choices = new ParticipantChoices();
      user.setParticipantChoices(choices);
    }
    List<ParticipantGroupConfig> list = choices.getParticipantGroupConfig();
    ParticipantGroupConfig choice = null;
    for (ParticipantGroupConfig c : list) {
      if (c.getGroupID().intValue() == groupID) {
        choice = c;
        break;
      }
    }
    if (choice == null) {
      choice = new ParticipantGroupConfig();
      choice.setGroupID(BigInteger.valueOf(groupID));
      list.add(choice);
    }
    return choice;
  }

  private List<List<Map<String, Object>>> executeProcedure(int vennue, java.util.Date date) {
    String sql = "call get_solver_data(?, ?);";
    if (date == null) {
      Calendar calendar = Calendar.getInstance();
      date = new java.util.Date(calendar.getTime().getTime());
    }
    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
    List<List<Map<String, Object>>> sets = null;
    try {
      CallableStatement stmt = conn.prepareCall(sql);
      stmt.setDate(1, sqlDate);
      stmt.setInt(2, vennue);

      boolean results = stmt.execute();

      //Loop through the available result sets.
      sets = new ArrayList<List<Map<String, Object>>>();
      while (results) {
        ResultSet rs = stmt.getResultSet();
        List<Map<String, Object>> setList = convertResultSetToList(rs);
        sets.add(setList);
        results = stmt.getMoreResults();
      } 
      stmt.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sets;
  }

  private List<Map<String,Object>> convertResultSetToList(ResultSet rs) throws SQLException {
    ResultSetMetaData md = rs.getMetaData();
    int columns = md.getColumnCount();
    
    List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
    while (rs.next()) {
      Map<String,Object> row = new HashMap<String, Object>(columns);
      for (int i=1; i<=columns; ++i) {
        row.put(md.getColumnName(i), rs.getObject(i));
      }
      list.add(row);
    }
    return list;
  }

  public static String decrypt(byte[] rawData) {
    String encryptionKey = System.getenv("ENCRYPTION_KEY");
    String output = null;

    String json = new String(Base64.decodeBase64(rawData));
    JsonObject root = (new JsonParser().parse(json)).getAsJsonObject();
    byte[] ivBytes = Base64.decodeBase64(root.get("iv").getAsString());
    byte[] data = Base64.decodeBase64(root.get("value").getAsString());
    try {
      // These decryption settings are compatible with the PHP encryption
      Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding");
      SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
      IvParameterSpec iv = new IvParameterSpec(ivBytes);
      cipher.init(Cipher.DECRYPT_MODE, key, iv);
      output = new String(cipher.doFinal(data), "UTF-8");
    } catch (NoSuchAlgorithmException ex) {
      ex.printStackTrace();
    } catch (InvalidKeyException ex) {
      ex.printStackTrace();
    } catch (IllegalBlockSizeException ex) {
      ex.printStackTrace();
    } catch (NoSuchPaddingException ex) {
      ex.printStackTrace();
    } catch (InvalidAlgorithmParameterException ex) {
      ex.printStackTrace();
    } catch (BadPaddingException ex) {
      ex.printStackTrace();
    } catch (UnsupportedEncodingException ex) {
      ex.printStackTrace();      
    }
    return output;
  }
}
