/**
 *
 */

package com.venngroups.models;

import java.math.*;
import com.venngroups.models.config.ActivityConfig;

class ActivityInstance
{
    public static BigInteger PARTICIPANT = BigInteger.valueOf(1);
    public static BigInteger COORDINATOR = BigInteger.valueOf(11);
    public static BigInteger OBSERVER = BigInteger.valueOf(21);
    
    public static BigInteger NULL_ACTIVITY = BigInteger.valueOf(-1);

    public static BigInteger PLAYER_DIVERSITY = BigInteger.valueOf(1);
    public static BigInteger FRIEND = BigInteger.valueOf(2);
    public static BigInteger FOE = BigInteger.valueOf(3);

    private int _instance;
    private BigInteger _roleID;
    private BigInteger _time;
    private ActivityConfig _config;
    
    public ActivityInstance(int instance, ActivityConfig config, BigInteger time, BigInteger roleID)
    {
    	_config = config;
    	_instance = instance;
        _time = time;
        _roleID = roleID;
    }

    public int getInstance()
    {
	   return _instance;
    }

    public ActivityConfig getConfig()
    {
	   return _config;
    }

    public BigInteger getTime()
    {
        return _time;
    }

    public BigInteger getRoleID()
    {
        return _roleID;
    }

    public String toString()
    {
    	if (_config == null)
    	    return "--Unassigned people";
    	return String.format("--%d %d", _config.getActivityID(), _instance);
    }
}
