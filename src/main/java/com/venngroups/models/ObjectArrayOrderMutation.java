package com.venngroups.models;

import java.util.*;
import org.uncommons.maths.number.*;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;

/**
 * A special mutation implementation that instead of changing the
 * genes of the candidate, re-orders them.  A single mutation involves
 * swapping a random element in the array with the element immediately
 * after it.  This operation can either apply a fixed number of
 * mutations to each candidate or it can draw values from a random
 * sequence, typically a poisson distribution (see
 * {@link org.uncommons.maths.random.PoissonGenerator}), to determine how
 * many mutations to apply.
 * @param <T> The component type of the lists that are mutated.
 */
public class ObjectArrayOrderMutation<T> implements EvolutionaryOperator<T[]>
{
    private final NumberGenerator<Integer> mutationCountVariable;
    private final NumberGenerator<Integer> mutationAmountVariable;

    /**
     * Default is one mutation per candidate.
     */
    public ObjectArrayOrderMutation()
    {
        this(1, 1);
    }

    /**
     * @param mutationCount The constant number of mutations
     * to apply to each individual in the population.
     * @param mutationAmount The constant number of positions by
     * which an array element will be displaced as a result of mutation.
     */
    public ObjectArrayOrderMutation(int mutationCount, int mutationAmount)
    {
        this(new ConstantGenerator<Integer>(mutationCount),
	     new ConstantGenerator<Integer>(mutationAmount));
    }


    /**
     * Typically the mutation count will be from a Poisson distribution.
     * The mutation amount can be from any discrete probability distribution
     * and can include negative values.
     * @param mutationCount A random variable that provides a number
     * of mutations that will be applied to each individual.
     * @param mutationAmount A random variable that provides a number
     * of positions by which to displace an element when mutating.
     */
    public ObjectArrayOrderMutation(NumberGenerator<Integer> mutationCount,
				    NumberGenerator<Integer> mutationAmount)
    {
        this.mutationCountVariable = mutationCount;
        this.mutationAmountVariable = mutationAmount;
    }


    public List<T[]> apply(List<T[]> selectedCandidates, Random rng)
    {
	int candidateLen = selectedCandidates.size();
	int arrayLen = selectedCandidates.get(0).length;
        List<T[]> result = new ArrayList<T[]>(candidateLen);	
	for (int i=0; i < candidateLen; i++)
        {
	    T[] candidate = selectedCandidates.get(i);
	    // Create the most specific-type arrays possible.
	    T[] newCandidate = Arrays.copyOf(candidate, candidate.length);
	    System.arraycopy(candidate, 0, newCandidate, 0, arrayLen);
            int mutationCount = Math.abs(mutationCountVariable.nextValue());
            for (int j = 0; j < mutationCount; j++)
            {
                int fromIndex = rng.nextInt(newCandidate.length);
                int mutationAmount = mutationAmountVariable.nextValue();
                int toIndex = (fromIndex + mutationAmount) % candidate.length;
                if (toIndex < 0)
                {
                    toIndex += candidate.length;
                }
                // Swap the randomly selected element with the one that is the
                // specified displacement distance away.
		newCandidate[fromIndex] = candidate[toIndex];
		newCandidate[toIndex] = candidate[fromIndex];
            }
            result.add(newCandidate);
        }
        return result;
    }
}
