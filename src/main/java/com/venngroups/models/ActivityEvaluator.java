/**
 *
 */

package com.venngroups.models;

import java.util.*;
import java.math.*;
import org.uncommons.watchmaker.framework.*;
import com.venngroups.models.config.*;
import com.venngroups.models.Solver;
import org.apache.commons.collections.CollectionUtils;

public class ActivityEvaluator implements FitnessEvaluator<List<BigInteger>>
{
  public static double 
    LargeGroupBonus = 3.0,
    BadAssignmentPenalty = -100.0,
    BadWorkgroupPenalty = -100.0,
    BadParticipantCountPenalty = -100.0,
    MissingCoordinatorPenalty = -100.0,
    TooManyInstancesPenalty = -100.0,
    NoAssignmentPenalty = -3.0,
    TooFewPlayersPenalty = -10.0,
    PlayerDiversityPenalty = -0.1,
    FriendBonus = 0.1,
    FoePenalty = -0.2,
    ActivityDiversityPenalty = -0.4;
  public static int engineGeneration = 1;

  public boolean verbose = false, updateConfig = false;
  public Config config;
  public List<ActivityInstance> slots;
  public Map<BigInteger, ParticipantConfig> userMap;
  public Map<BigInteger, ActivityConfig> activityMap;
  public Map<BigInteger, Set<BigInteger>> coordinatorMap;
  public Map<BigInteger, Map<Integer, BigDecimal>> funnessMap;
  public Map<BigInteger, Map<BigInteger, BigInteger>> pairMap;
  public Map<BigInteger, List<BigInteger>> workgroupMap;
  private TreeMap<BigInteger, Set<Integer>> times;

  private Random random;

  public ActivityEvaluator(Config conf, List<ActivityInstance> instances, boolean updateConf) {
    config = conf;
    slots = instances;
    updateConfig = updateConf;
    random = new Random();

    userMap = ConfigHelper.getUserMap(config);
    coordinatorMap = ConfigHelper.getCoordinatorMap(config);

    times = new TreeMap<BigInteger, Set<Integer>>();
    funnessMap = new HashMap<BigInteger, Map<Integer, BigDecimal>>();
    activityMap = new HashMap<BigInteger, ActivityConfig>();
    List<ActivityConfig> activityConfigs = config.getActivities().getActivityConfig();
    for (ActivityConfig a : activityConfigs) {
      BigInteger activityID = a.getActivityID();
      activityMap.put(activityID, a);
      Map<Integer, BigDecimal> ratings = new HashMap<Integer, BigDecimal>();
      ActivityRatings ratingConfig = a.getActivityRatings();
      if (ratingConfig != null) {
        List<ActivityRatingConfig> ratingConfigs = ratingConfig.getActivityRatingConfig();
        for (ActivityRatingConfig r : ratingConfigs) {
          BigInteger participationCountObject = r.getParticipantCount();
          Integer count = (participationCountObject == null) ? 5 : participationCountObject.intValue();
          BigDecimal fun = r.getRating();
          ratings.put(count, fun);
        }
        funnessMap.put(activityID, ratings);
      }
    }

    for (ActivityInstance a : slots) {
      BigInteger startTime = a.getTime();
      Set<Integer> instancesSet = times.get(startTime);
      if (instancesSet == null) {
        instancesSet = new HashSet<Integer>();
        times.put(startTime, instancesSet);
      }
    }

    pairMap = new HashMap<BigInteger, Map<BigInteger, BigInteger>>();
    List<PlayerPair> pairs = config.getPlayerPairs().getPlayerPair();
    for (PlayerPair p : pairs) {
      BigInteger userID1 = p.getUserID1();
      BigInteger userID2 = p.getUserID2();
      BigInteger typeID = p.getType();
      Map<BigInteger, BigInteger> map = pairMap.get(userID1);
      if (map == null) {
        map = new HashMap<BigInteger, BigInteger>();
        pairMap.put(userID1, map);
      }
      map.put(userID2, typeID);
    }

    workgroupMap = new HashMap<BigInteger, List<BigInteger>>();
    Workgroups groups = config.getWorkgroups();
    List<Workgroup> workgroups = (groups == null) ? new ArrayList<Workgroup>() : groups.getWorkgroup();
    for (Workgroup w : workgroups) {
      BigInteger workgroupID = w.getWorkgroupID();
      BigInteger parentID = w.getParentID();
      List<BigInteger> list = workgroupMap.get(workgroupID);
      if (list == null) {
        list = new ArrayList<BigInteger>();
        workgroupMap.put(workgroupID, list);
      }
      list.add(workgroupID);
      List<BigInteger> parentList = workgroupMap.get(parentID);
      if (parentList == null) {
        parentList = new ArrayList<BigInteger>();
        workgroupMap.put(workgroupID, parentList);
      }
      parentList.add(workgroupID);
    }
  } 

  public double getFitness(List<BigInteger> candidate, List<? extends List<BigInteger>> population)
  {
    // Assemble list of participants
    int len = slots.size(), participantCount = 0, oldInstance = -1, instance, tentativeCount = 0;
    double fitness = 100.0 * len;
    BigInteger activityID, oldActivityID = null, time, oldTime = null, roleID, oldRoleID = null;
    ActivityInstance activity, oldActivity = null;
    List<BigInteger> activityUsers = new ArrayList<BigInteger>();
    Map<BigInteger, Integer> validActivities = new HashMap<BigInteger, Integer>();
    for (int i=0; i < len; i++) {
      activity = slots.get(i);
      instance = activity.getInstance();
      time = activity.getTime();
      roleID = activity.getRoleID();
      ActivityConfig activityConf = activity.getConfig();
      BigInteger userID = candidate.get(i);
      if (userID == null)
        continue;
      activityID = (activityConf == null) ? BigInteger.ZERO : activityConf.getActivityID();
      // Process activity fitness after a new activity has been reached
      // No need to do this for the last activity since that's the unassigned group
      if (i > 0 && (!activityID.equals(oldActivityID) || !roleID.equals(oldRoleID)
                    || !time.equals(oldTime) || instance != oldInstance)) {
        // Are there a good number of participants in the activity?
        int userCount = activityUsers.size();
        if (userCount > 0) {
          for (BigInteger uID : activityUsers) {
            fitness += getParticipantFitness(uID, oldActivity, userCount - tentativeCount);
          }
          fitness += userCount * getActivityFitness(oldActivity, activityUsers, validActivities, tentativeCount);
        }
        activityUsers.clear();
        tentativeCount = 0;
      }     
      // For unassigned slots, calculate participant fitness now since we won't hit the above section again
      if (activityConf == null) {
        fitness += getParticipantFitness(userID, activity, 0);
      }
      // Add users to activity *after* old activity has been processed
      if (userID.intValue() > 0) {
        activityUsers.add(userID);
        BigInteger tentative = userMap.get(userID).getTentative();
        if (tentative != null && tentative.intValue() == 1)
          tentativeCount++;
      }
      oldActivityID = activityID;
      oldInstance = instance;
      oldActivity = activity;
      oldTime = time;
      oldRoleID = roleID;
    }
    // Are there too many instances of any activity?
    List<ActivityConfig> activityConfigs = config.getActivities().getActivityConfig();
    for (ActivityConfig activityConf : activityConfigs) {
      BigInteger maxInstancesObject = activityConf.getMaxInstances();
      if (maxInstancesObject == null)
        continue;
      int maxInstances = maxInstancesObject.intValue();
      BigInteger durationObject = activityConf.getDuration();
      if (durationObject == null)
        continue;
      long duration = durationObject.longValue();
      countInstancesForActivity(candidate, activityConf);
      NavigableSet<BigInteger> timeSlots = times.navigableKeySet();
      int i=0; 
      for (BigInteger firstSlot : timeSlots) {
        long firstEndTime = firstSlot.longValue() + duration;
        int count = times.get(firstSlot).size();
        int j=0;
        for (BigInteger nextSlot : timeSlots) {
          if (j <= i)
            continue;
          if (nextSlot.longValue() >= firstEndTime) 
            break;
          count += times.get(nextSlot).size();
          j++;
        }
        if (count > maxInstances) {
          fitness += TooManyInstancesPenalty;
          if (verbose == true)
            System.out.format("too many instances of activity %d%n", activityConf.getActivityID());
        }
        i++;
      }
    }
    // fitness must be >= 0
    return (fitness < 0.0) ? 0.0 : fitness;
  }

  public boolean isNatural()
  {
    return true;
  }

  public void setVerbose(boolean bVerbose)
  {
    verbose = bVerbose;
  }

  private void countInstancesForActivity(List<BigInteger> participants, ActivityConfig activity)
  {
    for (Set<Integer> i : times.values()) {
      i.clear();
    }
    int len = participants.size();
    BigInteger activityID = activity.getActivityID();
    for (int i=0; i < len; i++) {
      BigInteger p = participants.get(i);
      if (p == null || p.compareTo(BigInteger.ZERO) < 0)
        continue;
      ActivityInstance instance = slots.get(i);
      if (!instance.getRoleID().equals(ActivityInstance.PARTICIPANT))
        continue;
      ActivityConfig instanceConf = instance.getConfig();
      if (instanceConf == null)
        continue;
      if (!instanceConf.getActivityID().equals(activityID))
        continue;
      BigInteger time = instance.getTime();
      Integer instanceID = instance.getInstance();
      Set<Integer> instanceSet = times.get(time);
      instanceSet.add(instanceID);
    }
  }

  // Calculate multiplier for participant fitness penalties
  private double getActivityFitness(
      ActivityInstance activity, List<BigInteger> userList, Map<BigInteger, Integer> validActivities, int tentativeCount
    )
  {
    Integer participantCount = userList.size();
    double fitness = 0.0;
    ActivityConfig activityConf = activity.getConfig();
    BigInteger activityID = activityConf.getActivityID();

    if (participantCount.intValue() == 0)
      return 0.0;

    if (activityConf.getActivityID().equals(ActivityInstance.NULL_ACTIVITY))
      return 0.0;

    if (activity.getRoleID().equals(ActivityInstance.OBSERVER)) {
      Integer validActivity = validActivities.get(activityID);
      if (validActivity == null) {
        if (verbose == true)
          System.out.format("observer for unscheduled activity %d%n", activityID);
        return BadAssignmentPenalty;
      }
      return 0.0;
    }

    if (participantCount.intValue() > 2) {
      fitness += LargeGroupBonus;
    }

    double scaling = (double) engineGeneration / 200.0;
    scaling = (scaling < 1.0) ? scaling : 1.0;
    int minParticipants = activityConf.getMinParticipants().intValue();
    if (participantCount.intValue() > 0 && participantCount.compareTo(minParticipants + tentativeCount) < 0) {
      if (verbose == true)
        System.out.format("wrong count (%d) for activity %d (min %d)%n", participantCount - tentativeCount, activityID, minParticipants);
      fitness += BadParticipantCountPenalty * scaling;
    }

    boolean foundCoordinator = false;
    BigInteger needsCoordinator = activityConf.getNeedsCoordinator();
    if (needsCoordinator != null && needsCoordinator.intValue() == 1) {
      for (BigInteger userID : userList) {
        Set<BigInteger> cMap = coordinatorMap.get(userID);
        if (cMap != null && cMap.contains(activityID)) {
          foundCoordinator = true;
          break;
        }
      }
      if (!foundCoordinator) {
        if (verbose == true)
          System.out.format("missing coordinator for activity %d%n", activityID);
        fitness += MissingCoordinatorPenalty;
      }
    }

    for (BigInteger u : userList) {
      Map<BigInteger, BigInteger> typeMap = pairMap.get(u);
      ParticipantConfig user = userMap.get(u);
      BigInteger workgroupLimit = user.getWorkgroupLimitID();
      List<BigInteger> workgroupLimitList = (workgroupLimit == null) ? null : workgroupMap.get(workgroupLimit);
      for (BigInteger u2 : userList) {
        // Check for bad relationship types
        if (typeMap != null) {
          BigInteger type = typeMap.get(u2);
          if (type != null) {
            if (type.equals(ActivityInstance.PLAYER_DIVERSITY)) {
              if (verbose == true)
                System.out.format("player diversity penalty for activity %d%n", activityID);
              fitness += PlayerDiversityPenalty;              
            } else if (type.equals(ActivityInstance.FRIEND)) {
              if (verbose == true)
                System.out.format("friend bonus for activity %d%n", activityID);
              fitness += FriendBonus;
            } else if (type.equals(ActivityInstance.FOE)) {
              if (verbose == true)
                System.out.format("foe penalty for activity %d%n", activityID);
              fitness += FoePenalty;
            }
          }
        }
        // Check for bad workgroup types
        if (workgroupLimitList != null) {
          ParticipantConfig user2 = userMap.get(u2);
          BigInteger workgroup = user2.getWorkgroupID();
          if (workgroup != null && workgroupMap.containsKey(workgroup)) {
            if (!workgroupLimitList.contains(workgroup)) {
              if (verbose == true)
                System.out.format("workgroup penalty for activity %d (user %d limits user %d)%n", activityID, u, u2);
              fitness += BadWorkgroupPenalty;
            }
          }
        } //if
      } //for userList
    } //for userList

    if (activityID.compareTo(BigInteger.ZERO) > 0 && participantCount - tentativeCount >= minParticipants) 
    {
      Map<Integer, BigDecimal> fMap = funnessMap.get(activityID);
      if (fMap == null) {
        System.out.format("missing funness ratings for activity %d%n", activityID);
        return BadAssignmentPenalty;
      }
      BigDecimal funness;
      if (tentativeCount == 0) {
        funness = fMap.get(participantCount);
        if (funness.doubleValue() == 0.0)
          funness = BigDecimal.valueOf(BadParticipantCountPenalty * scaling);
      } else {
        BigDecimal worstFunness = null;
        int count = participantCount.intValue();
        for (int i=count-tentativeCount; i <= count; i++) {
          funness = fMap.get(Integer.valueOf(i));
          if (funness != null) {
            if (funness.doubleValue() == 0.0)
              funness = BigDecimal.valueOf(BadParticipantCountPenalty * scaling);
            if (worstFunness == null || funness.compareTo(worstFunness) < 0) {
              worstFunness = funness;
            }
          }
        }
        funness = worstFunness;
      }
      if (funness != null) {
        if (verbose == true)
          System.out.format("funness bonus for activity %d%n", activityID);
        fitness += funness.doubleValue();
        validActivities.put(activityID, 1);
      }
    }
    return fitness;
  }

  private double getParticipantFitness(BigInteger userID, ActivityInstance activity, int activityCount)
  {
    double fitness = 0.0, solutionMismatch = 1.0, agreeableScale = 0.3;
    ActivityConfig activityConf = activity.getConfig();
    BigInteger roleID = activity.getRoleID();
    ParticipantConfig user = userMap.get(userID);
    if (user == null)
      return 0.0;
    BigDecimal oldMismatchObject = user.getRecentSolutionMismatch();
    double oldMismatch = (oldMismatchObject == null) ? 1.0 : oldMismatchObject.doubleValue();
    if (oldMismatch < 1.0)
      oldMismatch = 1.0;

    if (verbose == true)
      System.out.format("===USER %d%n", user.getUserID());
    ParticipantChoices pChoices= user.getParticipantChoices();
    List<ParticipantChoiceConfig> choices = pChoices.getParticipantChoiceConfig();
    if (choices.size() > 2)
      agreeableScale = 1.0;
    if (activityConf == null) {
      if (verbose == true)
        System.out.format("%d not assigned to activity%n", user.getUserID());
      CommittedActivity commitment = user.getCommittedActivity();
      if (commitment != null) {
        if (verbose == true)
          System.out.format("Assignment fails commitment for user %d%n", user.getUserID());
        fitness += BadAssignmentPenalty;
      }
      BigInteger allowNonSolutions = user.getAllowNonSolutions();
      if (allowNonSolutions == null || allowNonSolutions.intValue() == 0) {
        fitness += NoAssignmentPenalty;
        solutionMismatch = 2.0;
      }
    } else {
      BigInteger activityID = activityConf.getActivityID();
      BigInteger activityTime = activity.getTime();
      int instance = (Integer) activity.getInstance();
      CommittedActivity commitment = user.getCommittedActivity();
      if (commitment != null) {
        if (!commitment.getActivityID().equals(activityID) || !commitment.getStartTime().equals(activityTime)
            || commitment.getInstance().intValue() != instance) {
          if (verbose == true)
            System.out.format("Assignment fails commitment for user %d%n", user.getUserID());
          fitness += BadAssignmentPenalty;
        } else {
          if (verbose == true)
            System.out.format("Commitment fulfilled for user %d%n", user.getUserID());
        }
      }
      BigInteger minPlayers = user.getMinPlayers();
      if (minPlayers != null && activityCount < minPlayers.intValue() && !roleID.equals(ActivityInstance.OBSERVER)) {
        if (verbose == true)
          System.out.format("Too few players for user %d%n", user.getUserID());
        fitness += TooFewPlayersPenalty;
      } else {
        double attractivenessFitness = 0.0, attractivenessSum = 0.0;
        for (ParticipantChoiceConfig c : choices) {
          BigDecimal attractivenessObject = c.getAttractiveness();
          if (attractivenessObject == null)
            continue;
          double attractiveness = c.getAttractiveness().doubleValue();
          attractivenessSum += attractiveness;
          BigInteger activityIDChoice = c.getActivityID();
          BigInteger activityChoiceTime = c.getStartTime();
          if (activityIDChoice != null && activityChoiceTime != null
              && activityIDChoice.equals(activityID) && activityChoiceTime.equals(activityTime)) {
            BigInteger userroleID = c.getRoleID();
            BigInteger activityRoleID = activity.getRoleID();
            if (activityRoleID == null || userroleID == null)
              continue;
            if ((activityRoleID.equals(ActivityInstance.PARTICIPANT) && !userroleID.equals(ActivityInstance.OBSERVER))
                || (activityRoleID.equals(ActivityInstance.OBSERVER) && userroleID.equals(ActivityInstance.OBSERVER)))
            {
              if (attractiveness == 0.0) {
                attractivenessFitness = BadAssignmentPenalty;
              } else if (attractiveness > attractivenessFitness) {
                attractivenessFitness = attractiveness;
                if (verbose == true)
                  System.out.format("fitness for activity %d instance %d, role %d: %f (%f mismatch)%n", 
                    activityConf.getActivityID(), activity.getInstance(), userroleID, attractivenessFitness, oldMismatch);
              }
            }
          }
        }
        if (attractivenessFitness == 0.0) {
          List<ParticipantGroupConfig> gChoices = pChoices.getParticipantGroupConfig();
          if (gChoices.size() > 0)
            agreeableScale = 1.0;
          BigInteger groupID = activityConf.getGroupID();
          for (ParticipantGroupConfig c : gChoices) {
            if (c.getGroupID().equals(groupID) && c.getStartTime().equals(activityTime)) {
              attractivenessFitness = c.getAttractiveness().doubleValue();
              if (verbose == true)
                System.out.format("group fitness for activity %d instance %d: %f (%f mismatch)%n", 
                  activityConf.getActivityID(), activity.getInstance(), attractivenessFitness, oldMismatch);
              break;
            }
          }
        }
        int choicesSize = choices.size();
        if (choicesSize > 0)
          solutionMismatch += (attractivenessSum / choicesSize);
        if (attractivenessFitness > 0.0) {
          fitness += attractivenessFitness * oldMismatch;
          solutionMismatch -= attractivenessFitness;
          BigInteger recentActivity = user.getRecentActivityID();
          if (recentActivity != null) {
            if (recentActivity.equals(activityID)) {
              if (verbose == true)
                System.out.format("penalty for recent activity %d%n", activityConf.getActivityID());
              fitness += ActivityDiversityPenalty;
            } else {
              ActivityConfig recentActivityConfig = activityMap.get(recentActivity);
              ActivityCategories categoryConfig = activityConf.getActivityCategories();
              ActivityCategories recentCategoryConfig = null;
              if (recentActivityConfig != null)
                recentCategoryConfig = recentActivityConfig.getActivityCategories();
              if (categoryConfig != null && recentCategoryConfig != null) {
                List<BigInteger> categories = categoryConfig.getActivityCategory();
                List<BigInteger> recentCategories = recentCategoryConfig.getActivityCategory();
                if (categories != null && recentCategories != null) {
                  Collection intersect = CollectionUtils.intersection(categories, recentCategories);
                  if (!intersect.isEmpty()) {
                    if (verbose == true)
                      System.out.format("penalty for recent activity category%n");
                    fitness += ActivityDiversityPenalty;
                  }
                }
              }
            }
          } 
        } else {
          if (verbose == true)
            System.out.format("bad assignment for activity %d%n", activityConf.getActivityID());
          fitness += BadAssignmentPenalty * oldMismatch;
          solutionMismatch = 2.0;
        }
      }
    }
    if (updateConfig) {
      user.setRecentSolutionMismatch(BigDecimal.valueOf(solutionMismatch));
    }
    if (verbose == true)
      System.out.format("agreeable scale: %.2f!%n", agreeableScale);
    return fitness * agreeableScale;
  }
}
