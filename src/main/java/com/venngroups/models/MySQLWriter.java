package com.venngroups.models;

import java.sql.*;
import java.util.*;
import java.math.*;
import com.venngroups.models.*;
import com.venngroups.models.config.*;

public class MySQLWriter
{
  private static Connection conn;
  private static boolean verbose;

  public MySQLWriter() {
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (Exception ex) {
      System.out.println("Unable to load MySQL driver: " + ex);
    }        
    String dbServer = System.getenv("DB_SERVER");
    String dbDatabase = System.getenv("DB_DATABASE");
    String dbUser = System.getenv("DB_USER");
    String dbPassword = System.getenv("DB_PASSWORD");
    String connStr = String.format("jdbc:mysql://%s/%s?user=%s&password=%s", dbServer, dbDatabase, dbUser, dbPassword);
    try {
      conn = DriverManager.getConnection(connStr);
    } catch (SQLException ex) {
      System.out.println("Exception: " + ex.getMessage());
    }
  }

  public void writeSolution(List<ActivityInstance> slots, List<BigInteger> solution, Config config, java.util.Date date, 
      int eventID, boolean stdout) {
    this.verbose = stdout;
    if (date == null) {
      Calendar calendar = Calendar.getInstance();
      date = new java.util.Date(calendar.getTime().getTime());
    }
    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
    Map<BigInteger, ParticipantConfig> userMap = ConfigHelper.getUserMap(config);
    int total = solution.size(), oldInstance = -1, insertID = -1, totalWrites = 0;
    BigInteger oldActivityID = null, oldTime = null;
    for (int i=0; i < total; i++) {
      BigInteger userID = solution.get(i);
      if (userID.equals(BigInteger.valueOf(-1)))
        continue;
      ParticipantConfig userConf = userMap.get(userID);
      BigDecimal mismatch = userConf.getRecentSolutionMismatch();
      ActivityInstance activity = slots.get(i);
      ActivityConfig activityConf = activity.getConfig();
      if (activityConf == null) {
        if (this.verbose)
          System.out.format("Writing activity -1 for user %d.%n", userID);
        writeSolutionUser(-1, userID, ActivityInstance.PARTICIPANT, mismatch, sqlDate, eventID);
        continue;
      }
      BigInteger activityID = activityConf.getActivityID();
      BigInteger time = activity.getTime();
      BigInteger roleID = activity.getRoleID();
      int instance = activity.getInstance();
      if (!activityID.equals(oldActivityID)|| !time.equals(oldTime) || instance != oldInstance) {
        insertID = writeSolutionActivity(activityID, time, instance, sqlDate, eventID);
        if (this.verbose)
          System.out.format("Wrote solution activity %d.%n", insertID);
      }
      if (this.verbose)
        System.out.format("Writing activity %d for user %d.%n", activityID, userID);
      writeSolutionUser(insertID, userID, roleID, mismatch, sqlDate, eventID);
      totalWrites++;
      oldActivityID = activityID;
      oldTime = time;
      oldInstance = instance;
    }
    if (totalWrites == 0) {
      if (this.verbose)
        System.out.format("Writing null solution (no activities).%n");
      writeSolutionActivity(BigInteger.ZERO, BigInteger.ZERO, 0, sqlDate, eventID);      
    }
  }

  public void deleteSolution(int vennueID, java.util.Date date) {
    String sql = "call delete_solution(?, ?);";
    if (date == null) {
      Calendar calendar = Calendar.getInstance();
      date = new java.util.Date(calendar.getTime().getTime());
    }
    java.sql.Date sqlDate = new java.sql.Date(date.getTime());
    try {
      CallableStatement stmt = conn.prepareCall(sql);
      stmt.setInt(1, vennueID);
      stmt.setDate(2, sqlDate);
      boolean results = stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private int writeSolutionActivity(BigInteger activityID, BigInteger timeInt, int instance, java.sql.Date date, int eventID) {
    String sql = "call insert_solution_activity(?, ?, ?, ?, ?);";
    int insertID = -1;
    try {
      CallableStatement stmt = conn.prepareCall(sql);
      stmt.setInt(1, eventID);
      stmt.setInt(2, activityID.intValue());
      stmt.setDate(3, date);
      stmt.setLong(4, timeInt.longValue());
      stmt.setInt(5, instance);
      boolean results = stmt.execute();
      ResultSet rs = stmt.getResultSet();
      rs.next();
      insertID = rs.getInt(1);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return insertID;
  }

  private void writeSolutionUser(int activityID, BigInteger userID, BigInteger roleID, BigDecimal mismatch, 
      java.sql.Date date, int eventID) {
    String sql = "call insert_solution_user(?, ?, ?, ?, ?, ?);";
    double mismatch_val = (mismatch == null) ? 1.0 : mismatch.doubleValue();
    Calendar calendar = Calendar.getInstance();
    try {
      CallableStatement stmt = conn.prepareCall(sql);
      stmt.setInt(1, eventID);
      stmt.setInt(2, activityID);
      stmt.setDate(3, date);
      stmt.setInt(4, userID.intValue());
      stmt.setInt(5, roleID.intValue());
      stmt.setFloat(6, (float)mismatch_val);
      boolean results = stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
