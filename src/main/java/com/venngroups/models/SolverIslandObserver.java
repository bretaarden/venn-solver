package com.venngroups.models;

import java.math.*;
import java.util.*;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.islands.IslandEvolutionObserver;

public class SolverIslandObserver implements IslandEvolutionObserver<List<BigInteger>>
{
  public void populationUpdate(PopulationData<? extends List<BigInteger>> data)
  {
    double fitness = data.getBestCandidateFitness();
    int generation = data.getGenerationNumber();
    ActivityEvaluator.engineGeneration = generation;
    System.out.format("Generation %d: %f%n", generation, fitness);
    System.gc();
  }

  public void islandPopulationUpdate(int islandIndex, PopulationData<? extends List<BigInteger>> data)
  {
    return;
  }
}
