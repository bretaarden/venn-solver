/**
 * 
 */

package com.venngroups.models;

import java.io.*;
import java.math.*;
import java.lang.Math.*;
import java.util.*;
import java.util.regex.*;
import java.net.*;
import javax.xml.bind.*;
import org.uncommons.maths.number.*;
import org.uncommons.maths.random.*;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.factories.ListPermutationFactory;
import org.uncommons.watchmaker.framework.operators.*;
import org.uncommons.watchmaker.framework.islands.*;
import org.uncommons.watchmaker.framework.termination.*;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import com.venngroups.models.*;
import com.venngroups.models.config.*;

/**
 *
 */
public class Solver {
  private static int 
    StagnationLimit = 1280000,
    Population = 10000, 
    Islands = 8,
    Generations = 5,
    Emigrees = 10;
  private static double
    CrossoverRate = 0.05,
    CrossoverProbability = 0.25,
    MutationRate = 0.05,
    MutationSize = 1.0;
  private Config config;
  private boolean solutionExists;
  private int vennueID;

  /**
   * Read the search config from a file
   *
   * @param args
   */
  public Solver(InputStream configData) {
    try {
      JAXBContext jc = JAXBContext.newInstance("com.venngroups.models.config");
      Unmarshaller unmarshaller = jc.createUnmarshaller();
      config = (Config) unmarshaller.unmarshal(configData);
    } catch (JAXBException e) {
      System.out.println("XML parsing error: " + e);
      System.exit(1);
    }
  }

  public Solver(int vennue, Date date) {
    this.vennueID = vennue;
    MySQLReader reader = new MySQLReader();
    config = reader.readConfig(vennue, date);
    solutionExists = reader.hasExistingSolution(vennue, date);
  }

  public void solve(boolean stdout, boolean saveToDB, boolean sendEmails, boolean sendWarnings, Date date, String seedString, String solutionString) {
    // Build the gene pool from the config
    List<ActivityInstance> slots = getActivitySlots(config);
    List<BigInteger> participants = getParticipants(slots, config);
    List<BigInteger> solution = new ArrayList<BigInteger>();
    if (solutionString != null && !solutionString.isEmpty()) {
      System.out.println("Loading solution string.");
      loadSolutionString(solution, slots, solutionString);
    } else if (solutionExists && saveToDB) {
      System.out.println("A solution already exists. Exiting.");
      return;
    } else if (slots.size() > 0 && participants.size() > 0) {
      System.out.println("Running solver.");
      //solution = runGenerationalSolver(participants, slots, config, stdout);
      solution = runIslandSolver(participants, slots, config);
    }
    if (stdout == true) {
      printSolution(solution, slots);
      ActivityEvaluator eval = new ActivityEvaluator(config, slots, true);
      eval.setVerbose(true);
      System.out.format("Fitness: %f%n", eval.getFitness(solution, null));
    }
    if (saveToDB == true) {
      MySQLWriter writer = new MySQLWriter();
      writer.writeSolution(slots, solution, config, date, vennueID, stdout);
    }
    if (sendEmails == true) {
      triggerEmails(this.vennueID);
    }
    if (sendWarnings == true) {
      triggerWarnings(solution, slots);
    }
  }

  private void loadSolutionString(List<BigInteger> solution, List<ActivityInstance> slots, String solutionString) {
    BigInteger neg1 = BigInteger.valueOf(-1);
    int solutionSize = slots.size();
    for (int i=0; i < solutionSize; i++) {
      solution.add(neg1);
    }
    // userID:activityID_roleID,...
    Pattern pattern = Pattern.compile("(\\d+):(-?\\d+)(_\\d+)?,?");
    Matcher matcher = pattern.matcher(solutionString);
    while (matcher.find()) {
      BigInteger userID = BigInteger.valueOf(Integer.parseInt(matcher.group(1)));
      BigInteger activityID = BigInteger.valueOf(Integer.parseInt(matcher.group(2)));
      BigInteger roleID = BigInteger.valueOf(1);
      String roleString = matcher.group(3);
      if (roleString != null) {
        roleID = BigInteger.valueOf(Integer.parseInt(roleString.substring(1)));
      }
      for (int i=0; i < solutionSize; i++) {
        ActivityInstance instance = slots.get(i);
        BigInteger activityRoleID = instance.getRoleID();
        if (activityRoleID.equals(roleID) || 
            (activityRoleID.equals(ActivityInstance.PARTICIPANT) && roleID.equals(ActivityInstance.COORDINATOR)))
        {
          ActivityConfig config = instance.getConfig();
          BigInteger slotActivity = neg1;
          if (config != null)
            slotActivity = config.getActivityID();
          if (activityID.equals(slotActivity)) {
            if (solution.get(i).equals(neg1)) {
              solution.set(i, userID);
              break;
            }
          }
        }
      }
    }
  }

  private void triggerEmails (int vennueID) {
    String securityKey = System.getenv("EMAIL_KEY");
    String url = String.format("http://www.venngroups.com/email/sendsolutions?key=%s&vennue=%d", securityKey, vennueID);
    try {
      URLConnection connection = new URL(url).openConnection();
      connection.setRequestProperty("Accept-Charset", "UTF-8");
      InputStream response = connection.getInputStream();
    } catch (MalformedURLException ex) {
      ex.printStackTrace();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  private void triggerWarnings (List<BigInteger> solution, List<ActivityInstance> slots) {
    List<String> solvedOutList = new ArrayList<String>();
    BigInteger neg1 = BigInteger.valueOf(-1);
    int len = solution.size();
    for (int i = 0; i < len; i++) {
      BigInteger p = solution.get(i);
      if (p.equals(neg1))
        continue;
      if (slots.get(i).getConfig() != null)
        continue;
      solvedOutList.add(p.toString());
    }
    if (solvedOutList.size() == 0) {
      System.out.format("No warnings sent!%n");
      return;
    }

    String solvedOut = String.join(",", solvedOutList);
    String securityKey = System.getenv("EMAIL_KEY");
    String urlPattern = "https://www.venngroups.com/email/sendwarnings?key=%s&vennue=%d&users=%s";
    String url = String.format(urlPattern, securityKey, this.vennueID, solvedOut);
    try {
      URLConnection connection = new URL(url).openConnection();
      connection.setRequestProperty("Accept-Charset", "UTF-8");
      InputStream response = connection.getInputStream();
    } catch (MalformedURLException ex) {
      ex.printStackTrace();
    } catch (IOException ex) {
      ex.printStackTrace();
    }    
    System.out.format("Warnings sent for users %s%n", solvedOut);
  }

  public String outputConfig(Object pObject) {
    java.io.StringWriter sw = new StringWriter();
    try {
      JAXBContext jc = JAXBContext.newInstance("com.venngroups.models.config");
      Marshaller marshaller = jc.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      marshaller.marshal(pObject, sw);
    } catch (JAXBException e) {
      e.printStackTrace();      
    }
    String configXML = sw.toString();
    return configXML;
  }

  private static List<BigInteger> runGenerationalSolver(List<BigInteger> participants, List<ActivityInstance> slots, Config config, boolean stdout) 
  {
    Random randomizer = new MersenneTwisterRNG();
    NumberGenerator<Integer> crossoverRate =
      new DiscreteUniformGenerator(1, Math.max(1, (int) (slots.size() * CrossoverRate)), randomizer);
    NumberGenerator<Probability> crossoverProb = 
      new ConstantGenerator<Probability>(new Probability(CrossoverProbability));
    NumberGenerator<Integer> mutationRate =
      new DiscreteUniformGenerator(1, Math.max(2, (int) (slots.size() * MutationRate)), randomizer);
    NumberGenerator<Integer> mutationSize =
      new DiscreteUniformGenerator(1, Math.max(2, (int) (slots.size() * MutationSize)), randomizer);

    CandidateFactory<List<BigInteger>> factory =
      new ListPermutationFactory<BigInteger>(participants);
    List<EvolutionaryOperator<List<BigInteger>>> operators
        = new LinkedList<EvolutionaryOperator<List<BigInteger>>>();
    operators.add(new VennListCrossover(crossoverRate, crossoverProb, participants));
    operators.add(new ListOrderMutation<BigInteger>(mutationRate, mutationSize));
    EvolutionaryOperator<List<BigInteger>> evolutionPipeline
        = new EvolutionPipeline<List<BigInteger>>(operators);
    FitnessEvaluator<List<BigInteger>> evaluator = new ActivityEvaluator(config, slots, false);
    ActivityEvaluator.engineGeneration = 1;
    SelectionStrategy<Object> selector = new RouletteWheelSelection();
    EvolutionObserver<? super List<BigInteger>> observer = new SolverObserver();
    int stagnation = StagnationLimit / Population;
    int elite_cnt = Population / 5;

    GenerationalEvolutionEngine<List<BigInteger>> engine =
      new GenerationalEvolutionEngine<List<BigInteger>>(
        factory,
        evolutionPipeline,
        evaluator,
        selector,
        randomizer);
    engine.addEvolutionObserver(observer);
    List<EvaluatedCandidate<List<BigInteger>>> solutions;
    solutions = engine.evolvePopulation(
        Population,
        elite_cnt,      // who automatically survive
        new StaticStagnation(stagnation), // terminate after N generations of no progress
        new ElapsedTime(5 * 60 * 1000)    // or quit after 5 minutes
      ); 
    double bestFitness = solutions.get(0).getFitness();
    if (stdout == true) {
      for (int i=0; true; i++) {
        EvaluatedCandidate<List<BigInteger>> individual = solutions.get(i);
        System.out.format("%n%n===%nSolution %d: fitness %f%n", i, individual.getFitness());
        printSolution(individual.getCandidate(), slots);
        if (individual.getFitness() < bestFitness)
          break;
      }
    }
    return solutions.get(0).getCandidate();
  }

  private static List<BigInteger> runIslandSolver(List<BigInteger> participants, List<ActivityInstance> slots, Config config) 
  {
    Migration migrator = new RingMigration();
    Random randomizer = new MersenneTwisterRNG();
    CandidateFactory<List<BigInteger>> factory =
      new ListPermutationFactory<BigInteger>(participants);
    NumberGenerator<Integer> crossoverRate =
      new DiscreteUniformGenerator(1, Math.max(1, (int) (slots.size() * CrossoverRate)), randomizer);
    NumberGenerator<Probability> crossoverProb = 
      new ConstantGenerator<Probability>(new Probability(CrossoverProbability));
    NumberGenerator<Integer> mutationRate =
      new DiscreteUniformGenerator(1, Math.max(2, (int) (slots.size() * MutationRate)), randomizer);
    NumberGenerator<Integer> mutationSize =
      new DiscreteUniformGenerator(1, Math.max(2, (int) (slots.size() * MutationSize)), randomizer);
    List<EvolutionaryOperator<List<BigInteger>>> operators
        = new LinkedList<EvolutionaryOperator<List<BigInteger>>>();
    operators.add(new VennListCrossover(crossoverRate, crossoverProb, participants));
    operators.add(new ListOrderMutation<BigInteger>(mutationRate, mutationSize));
    EvolutionaryOperator<List<BigInteger>> evolutionPipeline
        = new EvolutionPipeline<List<BigInteger>>(operators);

    FitnessEvaluator<List<BigInteger>> evaluator = new ActivityEvaluator(config, slots, false);
    ActivityEvaluator.engineGeneration = 1;
    SelectionStrategy<Object> selector = new RouletteWheelSelection();
    IslandEvolutionObserver<? super List<BigInteger>> observer = new SolverIslandObserver();
    int stagnation = StagnationLimit / Population / Generations;
    int pop_cnt = Population / Islands;
    int elite_cnt = pop_cnt / 5;

    IslandEvolution<List<BigInteger>> engine =
      new IslandEvolution<List<BigInteger>>(
        Islands, // islands
        migrator,
        factory,
        evolutionPipeline,
        evaluator,
        selector,
        randomizer);
    engine.addEvolutionObserver(observer);
    List<BigInteger> solution = engine.evolve(
        pop_cnt,  // per island
        elite_cnt,      // who automatically survive
        Generations, // between migrations
        Emigrees,    // per island
        new StaticStagnation(stagnation), // terminate after N generations of no progress
        new ElapsedTime(5 * 60 * 1000)   // or quit after 5 minutes
      ); 
    return solution;
  }

  private static List<ActivityInstance> getActivitySlots(Config conf) {
    List<ParticipantConfig> participants = conf.getParticipants().getParticipantConfig();
    List<ActivityConfig> activities = conf.getActivities().getActivityConfig();

    // Add the maximum number of slots per activity
    List<ActivityInstance> slots = new ArrayList<ActivityInstance>();
    for (ActivityConfig a : activities) {
      BigInteger minSizeObject = a.getMinParticipants();
      BigInteger maxSizeObject = a.getMaxParticipants();
      BigInteger maxInstancesObject = a.getMaxInstances();
      if (minSizeObject == null || maxSizeObject == null || maxInstancesObject == null)
        continue;
      BigInteger needsCoordinator = a.getNeedsCoordinator();
      int minSize =  minSizeObject.intValue();
      int maxSize = maxSizeObject.intValue();
      int maxInstances = maxInstancesObject.intValue();
      int instanceTotal = 0;
      Map<BigInteger, Set<BigInteger>> activityTimes = countParticipantsForActivity(participants, a, ActivityInstance.PARTICIPANT);
      Map<BigInteger, Set<BigInteger>> groupTimes = countParticipantsForGroup(participants, a);
      for (Map.Entry<BigInteger, Set<BigInteger>> entry : activityTimes.entrySet()) {
        int instanceCount = 0;
        BigInteger time = entry.getKey();
        Set<BigInteger> activityPeople = entry.getValue();
        Set<BigInteger> groupPeople = groupTimes.get(time);
        if (groupPeople != null)
          activityPeople.addAll(groupPeople);
        int pCount = activityPeople.size();
        // Create as many allowed instances of each activity as there are 
        // multiples of interested individuals of the minimum requirement
        for (; pCount >= minSize && instanceCount < maxInstances; pCount -= minSize, instanceCount++) {
          ActivityInstance instance = new ActivityInstance(instanceCount, a, time, ActivityInstance.PARTICIPANT);
          int instanceSize = Math.min(maxSize, activityPeople.size());
          for (int i=0; i < instanceSize; i++) {
            slots.add(instance);
          }
        }
        instanceTotal += instanceCount;
      }
      if (instanceTotal > 0) {
        activityTimes = countParticipantsForActivity(participants, a, ActivityInstance.OBSERVER);
        for (Map.Entry<BigInteger, Set<BigInteger>> entry : activityTimes.entrySet()) {
          BigInteger time = entry.getKey();
          Set<BigInteger> observerPeople = entry.getValue();
          int oCount = observerPeople.size();
          ActivityInstance instance = new ActivityInstance(0, a, time, ActivityInstance.OBSERVER);
          for (int i=0; i < oCount; i++) {
            slots.add(instance);
          }
        }
      }
    }
    // Add empty slots to allow for imperfect solutions
    ActivityInstance noActivity = new ActivityInstance(0, null, BigInteger.ZERO, ActivityInstance.PARTICIPANT);
    int pCount = participants.size();
    for (int i=0; i < pCount; i++) {
      slots.add(noActivity);
    }
    //printSlots(slots);
    return slots;
  }

  private static void printSlots(List<ActivityInstance> slots)
  {
    for (ActivityInstance a : slots) {
      System.out.println(a.toString());
    }
  }

  private static Map<BigInteger, Set<BigInteger>> countParticipantsForGroup(
      List<ParticipantConfig> participants, ActivityConfig activity
    )
  {
    Map<BigInteger, Set<BigInteger>> times = new HashMap<BigInteger, Set<BigInteger>>();
    BigInteger groupID = activity.getGroupID();   
    for (ParticipantConfig p : participants) {
      BigInteger userID = p.getUserID();
      List<ParticipantGroupConfig> choices = p.getParticipantChoices().getParticipantGroupConfig();
      for (ParticipantGroupConfig c : choices) {
        if (c.getGroupID().equals(groupID)) {
          BigInteger time = c.getStartTime();
          Set<BigInteger> people = times.get(time);
          if (people == null) {
            people = new HashSet<BigInteger>();
            times.put(time, people);
          }
          people.add(userID);
        }
      }
    }
    return times;
  }

  private static Map<BigInteger, Set<BigInteger>> countParticipantsForActivity(
      List<ParticipantConfig> participants, ActivityConfig activity, BigInteger roleID
    )
  {
    Map<BigInteger, Set<BigInteger>> times = new HashMap<BigInteger, Set<BigInteger>>();
    BigInteger activityID = activity.getActivityID();
    for (ParticipantConfig p : participants) {
      BigInteger userID = p.getUserID();
      List<ParticipantChoiceConfig> choices = p.getParticipantChoices().getParticipantChoiceConfig();
      for (ParticipantChoiceConfig c : choices) {
        BigInteger activityIDChoice = c.getActivityID();
        if (activityIDChoice != null && activityIDChoice.equals(activityID)) {
          boolean match = false;
          BigDecimal rating = c.getAttractiveness();
          if (rating.floatValue() == 0.0)
            continue;
          BigInteger choiceRoleID = c.getRoleID();
          if (choiceRoleID == null)
             continue;
          if (roleID.equals(ActivityInstance.OBSERVER)) {
            if (choiceRoleID.equals(ActivityInstance.OBSERVER)) {
              match = true;
            }
          } else if (!choiceRoleID.equals(ActivityInstance.OBSERVER)) {
            match = true;
          }
          if (match) {
            BigInteger time = c.getStartTime();
            Set<BigInteger> people = times.get(time);
            if (people == null) {
              people = new HashSet<BigInteger>();
              times.put(time, people);
            }
            people.add(userID);
          }
        }
      }
    }
    return times;
  }

  private static List<BigInteger> getParticipants(List<ActivityInstance> slots, Config conf)
  {
    int slotCount = slots.size();
    List<BigInteger> participants = new ArrayList<BigInteger>();
    List<ParticipantConfig> people = conf.getParticipants().getParticipantConfig();
    int participantCount = people.size();
    int i = 0;
    for (; i < participantCount; i++) {
      participants.add(people.get(i).getUserID());
    }
    for (; i < slotCount; i++) {
      participants.add(BigInteger.valueOf(-1));
    }
    return participants;
  }

  public static void printSolution(List<BigInteger> solution, List<ActivityInstance> slots)
  {
    int participantCount = solution.size();
    for (int i=0; i < participantCount; i++) {
      BigInteger userObject = solution.get(i);
      if (userObject == null)
        continue;
      int userID = userObject.intValue();
      ActivityInstance activity = slots.get(i);
      ActivityConfig activityConf = activity.getConfig();
      int activityID = (activityConf == null) ? -1 : activityConf.getActivityID().intValue();
      int instance = activity.getInstance();
      BigInteger roleID = activity.getRoleID();
      BigInteger time = activity.getTime();
      System.out.format("activity %d, time %d, instance %d, role %d: user %d%n", activityID, time, instance, roleID, userID);
    }
  }
}
