/**
 *
 */

package com.venngroups.models;

import java.util.List;
import com.venngroups.models.config.ParticipantConfig;

public class ParticipantInstance
{
    private ParticipantConfig _config;
    private List<ActivityInstance> _activities;
    private List<ParticipantConfig> _people;
    
    public ParticipantInstance(ParticipantConfig config, 
      List<ActivityInstance> activities,
      List<ParticipantConfig> people)
    {
       _config = config;
       _activities = activities;
       _people = people;
    }

   public ParticipantConfig getConfig()
   {
       return _config;
   }

   public List<ActivityInstance> getActivities()
   {
       return _activities;
   }

   public List<ParticipantConfig> getPeople()
   {
       return _people;
   }
}
