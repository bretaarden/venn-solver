/**
 *
 * Helper class for data manipulation of Venn Solver configuration files.
 *
 */

package com.venngroups.models;

import java.util.*;
import java.math.*;
import com.venngroups.models.config.*;


public class ConfigHelper
{

  public static Map<BigInteger, ParticipantConfig> getUserMap(Config conf) 
  {
    Map<BigInteger, ParticipantConfig> map = new HashMap<BigInteger, ParticipantConfig>();
    List<ParticipantConfig> people = conf.getParticipants().getParticipantConfig();
    for (ParticipantConfig p : people) {
      BigInteger userID = p.getUserID();
      map.put(userID, p);
    }
    return map; 
  }

  public static Map<BigInteger, Set<BigInteger>> getCoordinatorMap(Config conf) 
  {
    Map<BigInteger, Set<BigInteger>> cMap = new HashMap<BigInteger, Set<BigInteger>>();
    List<ParticipantConfig> people = conf.getParticipants().getParticipantConfig();
    for (ParticipantConfig p : people) {
      BigInteger userID = p.getUserID();
      Set<BigInteger> set = new HashSet<BigInteger>();
      List<ParticipantChoiceConfig> choices = p.getParticipantChoices().getParticipantChoiceConfig();
      for (ParticipantChoiceConfig c : choices) {
        BigInteger choiceRoleID = c.getRoleID();
        if (choiceRoleID != null && choiceRoleID.equals(ActivityInstance.COORDINATOR)) {
          BigInteger activityID = c.getActivityID();
          set.add(activityID);
        }
      }
      cMap.put(userID, set);
    }
    return cMap;  
  }

}
