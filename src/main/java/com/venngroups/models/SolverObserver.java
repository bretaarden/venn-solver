package com.venngroups.models;

import java.math.*;
import java.util.*;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.EvolutionObserver;
import com.venngroups.models.ActivityEvaluator;

public class SolverObserver implements EvolutionObserver<List<BigInteger>>
{
  public void populationUpdate(PopulationData<? extends List<BigInteger>> data)
  {
    double fitness = data.getBestCandidateFitness();
    int generation = data.getGenerationNumber();
    ActivityEvaluator.engineGeneration = generation;
    System.out.format("Generation %d: %f%n", generation, fitness);
    System.gc();
  }
}
