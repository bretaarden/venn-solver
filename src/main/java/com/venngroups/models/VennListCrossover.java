package com.venngroups.models;

import java.util.*;
import java.math.*;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.operators.*;

/*
  The ListCrossover operator "mates" a candidate with another candidate.
  For Venn candidates, a crossover will likely result in an invalid solution,
    because participants will get duplicated or dropped.
  This crossover variant removes duplicate participants in random order.
  It also finds any missing candidates and inserts them into random empty slots.
  This preserves the power of the crossover operation while minimizing noise.
*/

public class VennListCrossover extends ListCrossover<BigInteger>
{
  BigInteger BIGMINUSONE = BigInteger.valueOf(-1);
  List<BigInteger> participants;

  public VennListCrossover(NumberGenerator<Integer> crossoverPointsVariable, 
                           NumberGenerator<Probability> crossoverProbabilityVariable,
                           List<BigInteger> participantList)
  {
    super(crossoverPointsVariable, crossoverProbabilityVariable);
    participants = participantList;
  }

  @Override
  protected List<List<BigInteger>> mate(List<BigInteger> parent1,
                                        List<BigInteger> parent2,
                                        int numberOfCrossoverPoints,
                                        Random rng)
  {
    List<List<BigInteger>> children = super.mate(parent1, parent2, numberOfCrossoverPoints, rng);
    Random rand = new Random();
    for (List<BigInteger> child : children) {
      // Gather participant location info
      Map<BigInteger, List<Integer>> participantMap = new HashMap<BigInteger, List<Integer>>();
      for (BigInteger p : participants) {
        if (p.compareTo(BigInteger.ZERO) > 0) {
          participantMap.put(p, new ArrayList<Integer>());
        }
      }
      for (int i=0; i < child.size(); i++) {
        BigInteger p = child.get(i);
        if (p.compareTo(BigInteger.ZERO) > 0) {
          participantMap.get(p).add(i);
        }
      }
      // Clear duplicate participants
      for (Map.Entry<BigInteger, List<Integer>> entry : participantMap.entrySet()) {
        List<Integer> map = entry.getValue();
        int size = map.size();
        if (size > 1) {
          int goodIdx = rand.nextInt(size);
          for (int i=0; i < size; i++) {
            if (i != goodIdx) {
              child.set(map.get(i), BIGMINUSONE);
            }
          }
        }
      }
      // Add missing participants
      for (Map.Entry<BigInteger, List<Integer>> entry : participantMap.entrySet()) {
        int size = child.size();
        List<Integer> map = entry.getValue();
        if (map.size() == 0) {
          for (boolean found=false; !found; ) {
            int loc = rand.nextInt(size);
            if (child.get(loc).compareTo(BIGMINUSONE) == 0) {
              child.set(loc, entry.getKey());
              found = true;
            }
          }
        }
      }
    }
    return children;
  }
}
