
package com.venngroups.models;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.TerminationCondition;

/**
* A TerminationCondition that halts evolution if fitness
* remains static for a specified number of generations.
*/
public class StaticStagnation implements TerminationCondition
{
  private final int generationLimit;
  private final boolean usePopulationAverage;

  private double lastFitness;
  private int staticGenerations;

  public StaticStagnation(int generationLimit)
  {
    this(generationLimit, false);
  }

  public StaticStagnation(int generationLimit,
                          boolean usePopulationAverage)
  {
    this.generationLimit = generationLimit;
    this.usePopulationAverage = usePopulationAverage;
    staticGenerations = 0;
    lastFitness = 0;
  }

  public boolean shouldTerminate(PopulationData<?> populationData)
  {
    double fitness = getFitness(populationData);
    if (isFitnessStatic(fitness)) {
      staticGenerations++;
    } else {
      staticGenerations = 0;
    }
    lastFitness = fitness;
    return staticGenerations >= generationLimit;
  }

  private double getFitness(PopulationData<?> populationData)
  {
    return usePopulationAverage
    ? populationData.getMeanFitness()
    : populationData.getBestCandidateFitness();
  }

  private boolean isFitnessStatic(double fitness)
  {
    return (fitness == lastFitness);
  }
}